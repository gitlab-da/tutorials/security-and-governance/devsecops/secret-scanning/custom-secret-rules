# Custom IRS Rules

This project contains secret detection ruleset configuration which adds addtional rulesets to the default GitLeaks configuration to detect Social Security Numbers (SSN).

It is used by the IRS Audits project to show how to specify a remote configuration file

## References

- [Pipeline Secret Detection Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#custom-rulesets)

- [Customize Rulesets Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/customize_rulesets.html)

- Project photo by [Tingey Injury Law Firm](https://unsplash.com/@tingeyinjurylawfirm?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/brown-wooden-tool-on-white-surface-veNb0DDegzE?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)